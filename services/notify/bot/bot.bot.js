const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(process.env.NOTIFY_EMPLOYEE_TOKEN,{
	polling:true
});

module.exports = {
	bot
}
