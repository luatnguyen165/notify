'use strict';

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
const _ = require('lodash');
const Cron = require('moleculer-cron');
module.exports = {
    name: 'notify',
    version: 1,
    mixins: [Cron],
    /**
     * Settings
     */
    settings: {

    },
    crons: [{
            name: 'notify.checkTimeChatCron',
            cronTime: '* 5 * * *', // every  5 hours
            onTick: async function() {
                await this.call('v1.notify.checkTimeChatCron');
            },
            runOnInit: () => {
                console.log('notify.checkTimeChatCron job is created');
            },
            timeZone: 'Asia/Ho_Chi_Minh'
        },
        {
            name: 'notify.CheckDeleteMessageCron',
            cronTime: '* 5 * * *', // every  5 hours
            onTick: async function() {
                await this.call('v1.notify.CheckDeleteMessageCron');
            },
            runOnInit: () => {
                console.log('notify.checkTimeChatCron job is created');
            },
            timeZone: 'Asia/Ho_Chi_Minh'
        },

    ],

    /**
     * Dependencies
     */
    dependencies: [],

    /**
     * Actions
     */
    actions: {
        CheckDeleteMessageCron: {
            handler: require('./actions/checkDeleteMessage.action')
        },
        checkTimeChatCron: {
            handler: require('./actions/checkChat.actions')
        },
        notifyBot: {
            params: {
                body: {
                    $$type: 'object',
                    message: 'string|required',
                    group: 'array|required'
                }

            },
            handler: require('./actions/notifyBot.action')
        }

    },

    /**
     * Events
     */
    events: {

    },

    /**
     * Methods
     */
    methods: {
        teleBotRegistEmpoyee: require('./method/teleEmpoyee.method'),
        MeAPI: require('./method/MeApiService')
    },

    /**
     * Service created lifecycle event handler
     */
    async created() {
        await this.teleBotRegistEmpoyee();
        _.set(this, 'MeAPI', new this.MeAPI({
            url: process.env.KYC_URI,
            publicKey: process.env.PUBLICKEYSANDBOX,
            privateKey: process.env.PRIVATEKEYSANDBOX,
            isSecurity: false,
            'x-api-client': 'app'
        }));
    },

    /**
     * Service started lifecycle event handler
     */
    async started() {

    },

    /**
     * Service stopped lifecycle event handler
     */
    async stopped() {

    }

};