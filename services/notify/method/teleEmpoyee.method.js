const _ = require('lodash');
const { bot } = require('../bot/bot.bot');
const Request = require('request');
const url = process.env.WALLET_URL;
const axios = require('axios');
const MeAPI = require('./MeApiService');
const Authorization = process.env.WALLET_TOKEN;

// const RedisService = config.redis();

async function PostRequest(urlRequest, body, headers = {}) {
    return new Promise((resolve, reject) => {
        try {
            Request.post({
                url: urlRequest,
                timeout: 50000,
                body: JSON.stringify(body),
                headers: {
                    Authorization,
                    ...headers
                }
            }, (err, httpResponse, response) => {
                if (err) {
                    reject(err);
                }
                resolve(httpResponse);
            });
        } catch (error) {
            reject(error);
        }
    });
}


let opt = {
    reply_markup: {
        inline_keyboard: [
            [{
                text: 'YES ☝️',
                callback_data: 'YES'
            }, {
                text: 'NO',
                callback_data: 'NO'
            }]
        ]
    }
};
let employeePolicy = `
	Tài liệu
	/Document
	/ChinhSach
	/ThongTin
`;
let policyList = ['/Document', '/ChinhSach', '/ThongTin', '/DocSDK', '/Docs', '/BIGBOY'];
let customerPolicy = `
			Tài liệu SDK
			/DocSDK
			Tài liệu Chi hộ
			/Document
			/Docs
`;
module.exports = async function() {

        Logger = this.logger.info;
        const response = {
            code: 9999,
            message: 'TeleEmployee kết nối thất bại vui lòng kết nối lại sau.'
        };

        try {
            bot.onText(/\/start (.+)/, (ctx, match) => {
                const chatId = ctx.chat.id;
                const respo = match[1];
                if (respo) {
                    bot.sendMessage(chatId, `Bạn muốn đăng ký với tên ${respo} Vui lòng đợi hệ thống kiểm tra.`);
                } else {
                    bot.sendMessage(chatId, 'Bạn chưa nhập tên cần đăng ký');
                }
            });
            bot.on('message', async ctx => {

                        try {
                            const chatId = ctx.chat.id;
                            if (_.startsWith('/start', ctx.text.split(' ')[0]) && ctx.text.split(' ').length === 1) {

                                await bot.sendMessage(ctx.chat.id, 'Bạn có phải nhân viên Payme không', opt);

                            } else if (_.startsWith('/start', ctx.text.split(' ')[0]) && ctx.text.split(' ').length > 1) {
                                if (ctx.chat.type === 'private') {
                                    const checkAccountTelegram = await this.broker.call('v1.notifyModel.findOne', [{
                                        telegramId: ctx.chat.id
                                    }]);
                                    const data = ctx.text.split(' ')[1];
                                    if (!checkAccountTelegram) {
                                        await this.broker.call('v1.notifyModel.create', [{
                                            telegramId: chatId,
                                            firstname: ctx.chat.first_name || '',
                                            username: ctx.chat.last_name || '',
                                            alias: data
                                        }]);
                                        bot.sendMessage(chatId, `Chúc Mừng bạn đăng Ký Thành Công với @${data}`);
                                    } else {
                                        bot.sendMessage(chatId, 'Tài Khoản này đã từng đăng ký trước đó');
                                    }
                                } else {
                                    const checkAccountTelegram = await this.broker.call('v1.notifyModel.findOne', [{
                                        telegramId: ctx.from.id
                                    }]);
                                    const data = ctx.text.split(' ')[1];
                                    if (!checkAccountTelegram) {
                                        await this.broker.call('v1.notifyModel.create', [{
                                            telegramId: ctx.from.id,
                                            firstname: ctx.from.first_name || '',
                                            username: ctx.from.username || '',
                                            groupName: ctx.chat.title,
                                            groupId: ctx.chat.id,
                                            alias: data
                                        }]);
                                        bot.sendMessage(chatId, `Chúc Mừng bạn đăng Ký Thành Công với @${data}`);
                                    } else {
                                        bot.sendMessage(chatId, 'Tài Khoản này đã từng đăng ký trước đó');
                                    }
                                }

                            } else if (_.startsWith('/follow', ctx.text.split(' ')[0]) && ctx.text.split(' ').length === 1) {


                                if (ctx.chat.type === 'private') {
                                    const objWhere = {
                                        telegramId: ctx.chat.id,
                                        message: '/follow',
                                        status: true
                                    };
                                    const followExists = await this.broker.call('v1.notifyGroupModel.findOne', [objWhere]);

                                    if (followExists) {
                                        bot.sendMessage(ctx.chat.id, 'Tài khoản đã theo dõi trước đó');
                                    }
                                    if (!followExists) {

                                        await this.broker.call('v1.notifyGroupModel.create', [{
                                            telegramId: ctx.chat.id,
                                            message: ctx.text,
                                            status: true
                                        }]);
                                        bot.sendMessage(ctx.chat.id, 'Tài khoản theo dõi thành công');
                                    }
                                } else if (ctx.chat.type === 'group') {
                                    const objWhere = {
                                        telegramId: ctx.from.id,
                                        message: '/follow',
                                        status: true
                                    };
                                    const followExists = await this.broker.call('v1.notifyGroupModel.findOne', [objWhere]);
                                    console.log(followExists);
                                    if (followExists) {
                                        bot.sendMessage(ctx.chat.id, 'Tài khoản đã theo dõi trước đó');
                                    }
                                    if (!followExists) {

                                        await this.broker.call('v1.notifyGroupModel.create', [{
                                            telegramId: ctx.from.id,
                                            message: ctx.text,
                                            groupId: ctx.chat.id,
                                            status: true
                                        }]);
                                        bot.sendMessage(ctx.chat.id, 'Tài khoản theo dõi thành công');
                                    }
                                }


                            } else if (ctx.text === '/unfollow') {
                                if (ctx.chat.type === 'private') {
                                    const objWhere = {
                                        telegramId: ctx.chat.id,
                                        message: '/follow',
                                        status: true
                                    };
                                    const unfollow = await this.broker.call('v1.notifyGroupModel.findMany', [objWhere]);
                                    unfollow.map(async(item) => {
                                        await this.broker.call('v1.notifyGroupModel.delete', [{
                                            id: item.id
                                        }]);
                                    });
                                    bot.sendMessage(ctx.chat.id, 'Hủy liên kết thành công');
                                } else {
                                    const objWhere = {
                                        telegramId: ctx.from.id,
                                        message: '/follow',
                                        status: true
                                    };
                                    const unfollow = await this.broker.call('v1.notifyGroupModel.findMany', [objWhere]);
                                    unfollow.map(async(item) => {
                                        await this.broker.call('v1.notifyGroupModel.delete', [{
                                            id: item.id
                                        }]);
                                    });
                                    bot.sendMessage(ctx.chat.id, 'Hủy liên kết thành công');
                                }




                            } else if (policyList.includes(ctx.text)) {
                                const chatId = ctx.chat.id;
                                //

                                switch (ctx.text) {
                                    case '/Document':
                                        await bot.sendMessage(chatId, 'http://googleLink');
                                        break;
                                    case '/ChinhSach':
                                        await bot.sendMessage(chatId, 'http://googleLink');
                                        break;
                                    case '/ThongTin':
                                        await bot.sendMessage(chatId, 'http://googleLink');
                                        break;
                                    default:
                                        break;
                                }
                            } else if (_.startsWith('/otp', ctx.text.split(' ')[0]) && ctx.text.split(' ').length <= 2) {
                                let phone = ctx.text.split(' ')[1];
                                if (phone.length === 10 && _.startsWith(phone, '0')) {
                                    phone = `84${phone.substring(1,10)}`;
                                }
                                const Logger = this.logger.info;
                                const response = {
                                    code: 1111000,
                                    message: 'OTP kết nối đang gặp sự cố vui lòng quay lại sau, xin cảm ơn'
                                };
                                try {
                                    // const ReciveList = process.env.RECEIVER ? process.env.RECEIVER.split(',') : [];
                                    const accountInfo = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{
<<<<<<< services/notify/method/teleEmpoyee.method.js
                                        $and: [{ phone}, {source: "LOGIN" }]

=======
                                        phone: phone
>>>>>>> services/notify/method/teleEmpoyee.method.js
                                    }, null, { sort: { createdAt: -1 } }]);
                                    Logger('Account infor active code ', accountInfo);
                                    if (_.isNull(accountInfo) === true) {
                                        await bot.sendMessage(chatId, 'bạn đang không có OTP nào');

                                    } else {
                                        const activeCode = _.get(accountInfo, 'code', null);
                                        // goi thang notifyTelegram ra dua du lieu cho no xu ly
                                        Logger('Account  active code ', activeCode);
                                        await bot.sendMessage(ctx.chat.id, `${`Sandbox Ewallet - 📞 OTP của bạn ${phone} là ${activeCode}. Xin hân hạnh phục vụ ⏳⌛️`}`);
						    }
					} catch (error) {
						Logger('[OTP] error', JSON.stringify(error));
						if (error === 'Moleculer') {
							response.code = error.code;
							response.message = error.message;
						}
						return response;
					}
				}else if(_.startsWith('/otp', ctx.text.split(' ')[0]) && ctx.text.split(' ')[2] ==='register' && ctx.text.split(' ').length > 1){
                    let phone = ctx.text.split(' ')[1];
                    if (phone.length === 10 && _.startsWith(phone, '0')) {
                        phone = `84${phone.substring(1,10)}`;
                    }
                    const Logger = this.logger.info;
                    const response = {
                        code: 1111000,
                        message: 'OTP kết nối đang gặp sự cố vui lòng quay lại sau, xin cảm ơn'
                    };
                    try {
                        // const ReciveList = process.env.RECEIVER ? process.env.RECEIVER.split(',') : [];
                        const accountInfo = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{

                            $and:[
                                {phone: phone},
                                {source: 'REGISTER'}
                            ]
                           
                        },null,{sort:{createdAt:-1}}]);
                        Logger('Account infor active code ', accountInfo);
                        if (_.isNull(accountInfo) === true) {
                            await bot.sendMessage(chatId, 'bạn đang không có OTP nào');

                        } else {
                            const activeCode = _.get(accountInfo, 'code', null);
                            // goi thang notifyTelegram ra dua du lieu cho no xu ly
                            Logger('Account  active code ', activeCode);
                            await bot.sendMessage(ctx.chat.id, `${`Sandbox Ewallet - 📞 OTP của bạn ${phone} là ${activeCode}. Xin hân hạnh phục vụ ⏳⌛️`}`);
                }
        } catch (error) {
            Logger('[OTP] error', JSON.stringify(error));
            if (error === 'Moleculer') {
                response.code = error.code;
                response.message = error.message;
            }
            return response;
        }

                }else if(_.startsWith('/otp', ctx.text.split(' ')[0]) && ctx.text.split(' ')[2] ==='forgot' && ctx.text.split(' ').length > 1){
                    let phone = ctx.text.split(' ')[1];
                    if (phone.length === 10 && _.startsWith(phone, '0')) {
                        phone = `84${phone.substring(1,10)}`;
                    }
                    const Logger = this.logger.info;
                    const response = {
                        code: 1111000,
                        message: 'OTP kết nối đang gặp sự cố vui lòng quay lại sau, xin cảm ơn'
                    };
                    try {
                        // const ReciveList = process.env.RECEIVER ? process.env.RECEIVER.split(',') : [];
                        const accountInfo = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{

                            $and:[
                                {phone: phone},
                                {source: 'FORGOT_PASSWORD'}
                            ]
                           
                        },null,{sort:{createdAt:-1}}]);
                        Logger('Account infor active code ', accountInfo);
                        if (_.isNull(accountInfo) === true) {
                            await bot.sendMessage(chatId, 'bạn đang không có OTP nào');

                        } else {
                            const activeCode = _.get(accountInfo, 'code', null);
                            // goi thang notifyTelegram ra dua du lieu cho no xu ly
                            Logger('Account  active code ', activeCode);
                            await bot.sendMessage(ctx.chat.id, `${`Sandbox Ewallet - 📞 OTP của bạn ${phone} là ${activeCode}. Xin hân hạnh phục vụ ⏳⌛️`}`);
                }
        } catch (error) {
            Logger('[OTP] error', JSON.stringify(error));
            if (error === 'Moleculer') {
                response.code = error.code;
                response.message = error.message;
            }
            return response;
        }

                }else if(_.startsWith('/otp', ctx.text.split(' ')[0]) && ctx.text.split(' ')[2] ==='sdk' && ctx.text.split(' ').length > 1){
                    let phone = ctx.text.split(' ')[1];
                    if (phone.length === 10 && _.startsWith(phone, '0')) {
                        phone = `84${phone.substring(1,10)}`;
                    }
                    const Logger = this.logger.info;
                    const response = {
                        code: 1111000,
                        message: 'OTP kết nối đang gặp sự cố vui lòng quay lại sau, xin cảm ơn'
                    };
                    try {
                        // const ReciveList = process.env.RECEIVER ? process.env.RECEIVER.split(',') : [];
                        const accountInfo = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{
                            $and:[
                                {phone: phone},
                                {source: 'LOGIN_SDK'}
                            ]
                           
                        },null,{sort:{createdAt:-1}}]);
                        Logger('Account infor active code ', accountInfo);
                        if (_.isNull(accountInfo) === true) {
                            await bot.sendMessage(chatId, 'bạn đang không có OTP nào');

                        } else {
                            const activeCode = _.get(accountInfo, 'code', null);
                            // goi thang notifyTelegram ra dua du lieu cho no xu ly
                            Logger('Account  active code ', activeCode);
                            await bot.sendMessage(ctx.chat.id, `${`Sandbox Ewallet - 📞 OTP của bạn ${phone} là ${activeCode}. Xin hân hạnh phục vụ ⏳⌛️`}`);
                }
        } catch (error) {
            Logger('[OTP] error', JSON.stringify(error));
            if (error === 'Moleculer') {
                response.code = error.code;
                response.message = error.message;
            }
            return response;
        }

                }else if(_.startsWith('/otp', ctx.text.split(' ')[0]) && ctx.text.split(' ')[2] ==='lock' && ctx.text.split(' ').length > 1){
                    let phone = ctx.text.split(' ')[1];
                    if (phone.length === 10 && _.startsWith(phone, '0')) {
                        phone = `84${phone.substring(1,10)}`;
                    }
                    const Logger = this.logger.info;
                    const response = {
                        code: 1111000,
                        message: 'OTP kết nối đang gặp sự cố vui lòng quay lại sau, xin cảm ơn'
                    };
                    try {
                        // const ReciveList = process.env.RECEIVER ? process.env.RECEIVER.split(',') : [];
                        const accountInfo = await this.broker.call('v1.ewalletActiveCodeModel.findOne', [{
                            $and:[
                                {phone: phone},
                                {source: 'LOCK_ACCOUNT'}
                            ]
                           
                        },null,{sort:{createdAt:-1}}]);
                        Logger('Account infor active code ', accountInfo);
                        if (_.isNull(accountInfo) === true) {
                            await bot.sendMessage(chatId, 'bạn đang không có OTP nào');

                        } else {
                            const activeCode = _.get(accountInfo, 'code', null);
                            // goi thang notifyTelegram ra dua du lieu cho no xu ly
                            Logger('Account  active code ', activeCode);
                            await bot.sendMessage(ctx.chat.id, `${`Sandbox Ewallet - 📞 OTP của bạn ${phone} là ${activeCode}. Xin hân hạnh phục vụ ⏳⌛️`}`);
                }
        } catch (error) {
            Logger('[OTP] error', JSON.stringify(error));
            if (error === 'Moleculer') {
                response.code = error.code;
                response.message = error.message;
            }
            return response;
        }

                }else if (_.startsWith('/addmoney', ctx.text.split(' ')[0]) && ctx.text.split(' ').length > 1) {
					let phone = ctx.text.split(' ')[1];
					
					if(phone.length === 10 && _.startsWith(phone,'0')){
						phone= `84${phone.substring(1,10)}`;
					}
				
					const accountId = await this.broker.call('v1.eWalletAccountModel.findOne',[{phone}]);
					if(!accountId){
						bot.sendMessage(chatId, 'Không Tìm thấy thông tin tài khoản vui lòng kiểm tra lại thông tin nhập vào');
					}
					const response = {
						successed: false,
						message: 'Nạp tiền vào Ví thất bại.',
						balance: null,
						supplierResponse: null
					};
					const urlRequest = `${url}/Wallet/Deposit`;
					const body = {
						accountId: accountId.id,
						amount: 50000000,
						description: 'Nạp tiền vào ví',
						referData: {}
					};
					try {
						const result = await PostRequest(urlRequest, body);
						
						response.supplierResponse = _.get(result, 'body', JSON.stringify(result));
						// console.log(await result);
						const bodyResponse = JSON.parse(result.body);
						console.log(bodyResponse);
						if (bodyResponse.code !== 1000) {
							bot.sendMessage(chatId, 'Nạp tiền vào ví PayME thất bại');
							Logger(`[WALLET_LIBRARY] => [Deposit] => [Params] : ${body}`);
							Logger(`[WALLET_LIBRARY] => [Deposit] => [Response] : ${result}`);
							response.message = _.get(bodyResponse, 'data.message', response.message);
							return response;
						}
						response.successed = true;
						response.message = 'Nạp tiền vào ví PayME thành công.';
						response.balance = _.get(bodyResponse, 'data.balance', null);
						bot.sendMessage(chatId, 'Nạp tiền vào ví PayME thành công.');
						return response;
					} catch (error) {
						Logger(`[WALLET_LIBRARY] => [WALLET_LIBRARY: ${urlRequest}] => [Exception] : ${error}`);
						response.supplierResponse = JSON.stringify({ errorMessage: error.message });
						return response;
					}
				}else if(_.startsWith('/kyc', ctx.text.split(' ')[0]) && ctx.text.split(' ').length > 1) {
					let phone = ctx.text.split(' ')[1];
					
					if(phone.length === 10 && _.startsWith(phone,'0')){
						phone= `84${phone.substring(1,10)}`;
					}
				
					const accountId = await this.broker.call('v1.eWalletAccountModel.findOne',[{phone}]);
					if(!accountId){
						bot.sendMessage(chatId, 'Không Tìm thấy thông tin tài khoản vui lòng kiểm tra lại thông tin nhập vào');
					}

                    const url = process.env.KYC_URI;
                    const meAPI = new MeAPI({
                        url,
                        publicKey: process.env.PUBLICKEYSANDBOX,
                        privateKey: process.env.PRIVATEKEYSANDBOX,
                        isSecurity: true,
                        // 'x-api-client': '754545943390'
                        'x-api-client': 'app'
                        // 'x-api-client': '080380980537'
                    });
                
                    const params2 = {
                        "phone": phone,
                        "kycInfo": {
                          "fullname": "Nguyễn Văn A",
                          "gender": "MALE",
                          "birthday": "2022-04-08T06:48:45.414Z",
                          "address": "Inazuma",
                          "identifyType": "CMND",
                          "identifyNumber": "2732",
                          "issuedAt": "2022-04-08T06:48:45.414Z",
                          "placeOfIssue": "Inazuma",
                          "video": "example.com",
                          "face": "example.com",
                          "image": {
                            "front": "example.com",
                            "back": "example.com"
                          }
                        }
                      };
                
                        const response2 = await meAPI.Post('/v3/Internal/KYC', params2, process.env.KYC_TOKEN);
                        if(response2.data.code === 1000){
                            bot.sendMessage(chatId, 'Kyc thành công');
                           
                         }else{
                            bot.sendMessage(chatId, 'Kyc thất bại');
                         }     
                    
				} else {
					if(ctx.chat.type==='private'){
						await this.broker.call('v1.notifyGroupModel.create', [{
							telegramId: ctx.chat.id,
							message: ctx.text
						}]);
					}else{
						await this.broker.call('v1.notifyGroupModel.create', [{
							telegramId: ctx.from.id,
							groupName:ctx.chat.title,
							message: ctx.text,
							groupId:ctx.chat.id
						}]);
					}

				}
			} catch (error) {
				Logger('[TeleEmployee bot] Kết nối thất bại error', JSON.stringify(error));
				if (error === 'MoleculerError') {
					response.code = error.code;
					response.message = error.message;
				}
				return response;
			}
		});
		bot.on('callback_query', ctx => {
			if (ctx.data == 'NO') {
				bot.sendMessage(ctx.message.chat.id, customerPolicy);
			} else {
				bot.sendMessage(ctx.message.chat.id, employeePolicy);
			}
		});
	} catch (error) {
		Logger('[TeleEmployee bot] Kết nối thất bại error', JSON.stringify(error));
		if (error === 'MoleculerError') {
			response.code = error.code;
			response.message = error.message;
		}
		return response;
	}


};