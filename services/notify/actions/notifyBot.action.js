const { asyncParallelForEach, BACK_OFF_RETRY } =require ('async-parallel-foreach');
const _ = require('lodash');
const { bot } = require('../bot/bot.bot');

module.exports = async function(ctx){
	const response = {
		code:908686,
		message:'NotifyBot xảy ra lỗi'
	};
	try {
		const payload = ctx.params.body;
		const message = _.get(payload,'message','');
		const group = _.get(payload,'group',[]);
		console.log(group,message);
		const listGroupReceiver =  await this.broker.call('v1.notifyModel.findMany',[{alias:{$in:group}}]);
		asyncParallelForEach(listGroupReceiver,100,async(item)=>{
			await bot.sendMessage(item.telegramId,message);
		});

	} catch (error) {
		if(error==='MoleculerError'){
			response.code = error.code;
			response.message = error.message;
		}
		return response;
	}


};
