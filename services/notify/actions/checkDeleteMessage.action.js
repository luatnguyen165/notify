const moment = require('moment');
module.exports =async function(ctx){
	const response = {
		code:868686,
		message:'Check chat message live xảy ra lỗi'
	};
	try {
		this.logger.warn('[checkDeleteMessage 5 hours] CRON >>>>');
		const checkTimeLive =  await this.broker.call('v1.notifyGroupModel.findMany',[{
			$and:[{status:{$ne:true}},{
				message:{$ne:'/follow'}
			}]
		},null,{sort:{id:-1}}]);

		checkTimeLive.map(async(item)=>{
			if(item.createdAt< moment(new Date()).add(-10,'hours')){
				await this.broker.call('v1.notifyGroupModel.delete',[{id:item.id}]);
			}
		});

	} catch (error) {
		if(error==='MoleculerError'){
			response.code = error.code;
			response.message = error.message;
		}
		return response;
	}

};
