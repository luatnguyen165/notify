const moment = require('moment');
module.exports =async function(ctx){
	const response = {
		code:868686,
		message:'Check chat message live xảy ra lỗi'
	};
	try {
		this.logger.info('[checkChat 5 minutes] CRON >>>>');
		const checkTimeLive =  await this.broker.call('v1.notifyGroupModel.findMany',[{},null,{sort:{id:-1}}]);
		if(checkTimeLive[0].createdAt < moment(new Date()).add(-5,'minutes')){
			const chatId = checkTimeLive[0].telegramId;
			const check =  await this.broker.call('v1.notifyModel.findOne',[{telegramId:chatId}]);
			if(!check){
				// bắn lên bot
				const CUSTOMER =  process.env.CUSTOMER?process.env.CUSTOMER.split(','):[];
				// tạo bot khác để bắn lên
				await this.broker.call('v1.notify.notifyBot',{body:{
					message:'Group A đã 5 phút rồi đối tác hỏi mà không ai reply của mấy má ơi !!!',
					group:CUSTOMER
				}});

			}
			return;
		}
	} catch (error) {
		if(error==='MoleculerError'){
			response.code = error.code;
			response.message = error.message;
		}
		return response;
	}

};
