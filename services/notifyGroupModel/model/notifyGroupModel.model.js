const mongoose = require('mongoose');
const _ = require('lodash');
autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	telegramId: {
		type: Number
	},
	groupName:{
		type:String
	},
	groupId:{
		type: Number
	},
	fullName:{
		type:String
	},
	message:{
		type: String
	},
	status:{
		type: Boolean
	},
	timeSender:{
		type: Date
	}

}, {
	collection: 'notifyGroupTelegram',
	versionKey: false,
	timestamps: true
});

Schema.index({ telegramId: 1 }, { sparse: true, unique: false });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
